package com.cjzy.view.provider;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.cjzy.common.CommonFactory;
import com.cjzy.common.exception.CustomerExistException;
import com.cjzy.model.Customer;
import com.cjzy.model.Provider;
import com.cjzy.service.CustomerService;
import com.cjzy.service.ProviderService;

public class ProviderAddPanel extends JPanel implements ActionListener{

	private JTextField providerNameTf;
	private JTextField addressTf;
	private JTextField shortsTf;
	private JTextField zipTf;
	private JTextField telTf;
	private JTextField faxTf;
	private JTextField contactsTf;
	private JTextField phoneTf;
	private JTextField emailTf;
	private JTextField bankTf;
	private JTextField accountTf;
	private ProviderService providerService = null;

	/**
	 * Create the panel.
	 */
	public ProviderAddPanel() {
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(screenSize.width/12,screenSize.height/20,screenSize.width/2,screenSize.height/2);
		setBounds(100, 100, 723, 463);
		JLabel lblNewLabel = new JLabel("供应商全称：");
		add(lblNewLabel);
		
		providerNameTf = new JTextField();
		add(providerNameTf);
		providerNameTf.setColumns(30);
		
		JLabel label = new JLabel("供应商地址：");
		add(label);
		
		addressTf = new JTextField();
		add(addressTf);
		addressTf.setColumns(30);
		
		JLabel label_1 = new JLabel("供应商简称：");
		add(label_1);
		
		shortsTf = new JTextField();
		add(shortsTf);
		shortsTf.setColumns(30);
		
		JLabel label_2 = new JLabel("供应商邮编：");
		add(label_2);
		
		zipTf = new JTextField();
		add(zipTf);
		zipTf.setColumns(30);
		
		JLabel label_3 = new JLabel("供应商电话：");
		add(label_3);
		
		telTf = new JTextField();
		add(telTf);
		telTf.setColumns(30);
		
		JLabel label_4 = new JLabel("传真：   ");
		add(label_4);
		
		faxTf = new JTextField();
		add(faxTf);
		faxTf.setColumns(30);
		
		JLabel label_6 = new JLabel("联系人：  ");
		add(label_6);
		
		contactsTf = new JTextField();
		add(contactsTf);
		contactsTf.setColumns(30);
		
		JLabel label_7 = new JLabel("联系人电话");
		add(label_7);
		
		phoneTf = new JTextField();
		add(phoneTf);
		phoneTf.setColumns(30);
		
		JLabel lblNewLabel_1 = new JLabel("  e-mail:");
		add(lblNewLabel_1);
		
		emailTf = new JTextField();
		add(emailTf);
		emailTf.setColumns(73);
		
		JLabel label_8 = new JLabel("开户银行：");
		add(label_8);
		
		bankTf = new JTextField();
		add(bankTf);
		bankTf.setColumns(30);
		
		JLabel label_9 = new JLabel("银行账户：");
		add(label_9);
		
		accountTf = new JTextField();
		add(accountTf);
		accountTf.setColumns(30);
		
		JPanel panel = new JPanel();
		add(panel);
		
		JButton saveBtn = new JButton("保存");
		saveBtn.addActionListener(this);
		
		JButton resetBtn = new JButton("重置");
		resetBtn.addActionListener(this);
		
		providerService = CommonFactory.getProviderService();
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(saveBtn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(resetBtn)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(saveBtn)
						.addComponent(resetBtn)))
		);
		panel.setLayout(gl_panel);
		
		JLabel label_5 = new JLabel("");
		add(label_5);
		
		
		
		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String target = e.getActionCommand();
		switch (target) {
		case "保存":
			if(providerNameTf.getText().trim().equals("")||
					addressTf.getText().trim().equals("")||
					shortsTf.getText().trim().equals("")||
					zipTf.getText().trim().equals("")||
					telTf.getText().trim().equals("")||
					faxTf.getText().trim().equals("")||
					contactsTf.getText().trim().equals("")||
					phoneTf.getText().trim().equals("")||
					emailTf.getText().trim().equals("")||
					bankTf.getText().trim().equals("")||
					accountTf.getText().trim().equals("")) {
				
				JOptionPane.showMessageDialog(null, "请填写全部信息","温馨提示",JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			//客户编号
			String id = providerService.getProviderId();
			//将客户添加到数据库
			Provider provider = new Provider(id, providerNameTf.getText().trim(), shortsTf.getText().trim(), addressTf.getText().trim(), 
					zipTf.getText().trim(), telTf.getText().trim(), faxTf.getText().trim(), contactsTf.getText().trim(), 
					phoneTf.getText().trim(), bankTf.getText().trim(), accountTf.getText().trim(), 
					emailTf.getText().trim(), 1);
			boolean result;
			
			result = providerService.addProvider(provider);
			
			if(result=true) {
				JOptionPane.showMessageDialog(null, "供应商信息添加成功","提示",JOptionPane.INFORMATION_MESSAGE);
				setNull();
			}else {
				JOptionPane.showMessageDialog(null, "供应商信息添加失败","警告",JOptionPane.WARNING_MESSAGE);
			}
			break;
		case "重置":
			setNull();
		default:
			break;
		}
	}
	/***
	 * 清空文本框
	 */
	private void setNull() {
		providerNameTf.setText(""); shortsTf.setText(""); addressTf.setText(""); 
		zipTf.setText("");telTf.setText(""); faxTf.setText(""); contactsTf.setText(""); 
		phoneTf.setText("");bankTf.setText(""); accountTf.setText(""); 
		emailTf.setText("");
	}

}
