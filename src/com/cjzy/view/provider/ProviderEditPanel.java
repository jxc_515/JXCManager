package com.cjzy.view.provider;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.cjzy.common.CommonFactory;
import com.cjzy.common.Constants;
import com.cjzy.model.Customer;
import com.cjzy.model.Item;
import com.cjzy.model.Provider;
import com.cjzy.service.CustomerService;
import com.cjzy.service.ProviderService;

public class ProviderEditPanel extends JPanel implements ActionListener{

	private JTextField ProviderNameTf;
	private JTextField addressTf;
	private JTextField shortsTf;
	private JTextField zipTf;
	private JTextField telTf;
	private JTextField faxTf;
	private JTextField contactsTf;
	private JTextField phoneTf;
	private JTextField emailTf;
	private JTextField bankTf;
	private JTextField accountTf;
	private ProviderService providerService;;
	private JComboBox comboBox;
	private Item selectedItem;

	/**
	 * Create the panel.
	 */
	public ProviderEditPanel() {
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblNewLabel = new JLabel("供应商全称：");
		add(lblNewLabel);

		ProviderNameTf = new JTextField();
		add(ProviderNameTf);
		ProviderNameTf.setColumns(30);

		JLabel label = new JLabel("供应商地址：");
		add(label);

		addressTf = new JTextField();
		add(addressTf);
		addressTf.setColumns(30);

		JLabel label_1 = new JLabel("供应商简称：");
		add(label_1);

		shortsTf = new JTextField();
		add(shortsTf);
		shortsTf.setColumns(30);

		JLabel label_2 = new JLabel("供应商邮编：");
		add(label_2);

		zipTf = new JTextField();
		add(zipTf);
		zipTf.setColumns(30);

		JLabel label_3 = new JLabel("供应商电话：");
		add(label_3);

		telTf = new JTextField();
		add(telTf);
		telTf.setColumns(30);

		JLabel label_4 = new JLabel("传真：   ");
		add(label_4);

		faxTf = new JTextField();
		add(faxTf);
		faxTf.setColumns(30);

		JLabel label_6 = new JLabel("联系人：  ");
		add(label_6);

		contactsTf = new JTextField();
		add(contactsTf);
		contactsTf.setColumns(30);

		JLabel label_7 = new JLabel("联系人电话");
		add(label_7);

		phoneTf = new JTextField();
		add(phoneTf);
		phoneTf.setColumns(30);

		JLabel lblNewLabel_1 = new JLabel("  e-mail:");
		add(lblNewLabel_1);

		emailTf = new JTextField();
		add(emailTf);
		emailTf.setColumns(75);

		JLabel label_8 = new JLabel("开户银行：");
		add(label_8);

		bankTf = new JTextField();
		add(bankTf);
		bankTf.setColumns(30);

		JLabel label_9 = new JLabel("银行账户：");
		add(label_9);

		accountTf = new JTextField();
		add(accountTf);
		accountTf.setColumns(30);

		JPanel panel = new JPanel();
		add(panel);

		JLabel label_5 = new JLabel("选择客户：");
		panel.add(label_5);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "\u6D4B\u8BD51", "\u6D4B\u8BD52", "\u5F69\u82722" }));
		add(comboBox);
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				selectAction();
			}
		});

		JButton button = new JButton("修改");
		add(button);
		button.addActionListener(this);

		JButton button_1 = new JButton("删除");
		add(button_1);
		button_1.addActionListener(this);

		JList list = new JList();
		add(list);

		providerService = CommonFactory.getProviderService();

		// 初始化客户下拉框
		initComboBox();
	}

	// 初始化下来框
	public void initComboBox() {

		List<Provider> list = providerService.findProviders(null);
		List<Item> items = new ArrayList<Item>();
		comboBox.removeAllItems();
		// 迭代处理数据
		for (Provider provider : list) {
			Item item = new Item();
			item.setId(provider.getId());
			item.setName(provider.getProviderName());
			System.out.println(item);
			System.out.println(provider);
			comboBox.addItem(item);
		}

	}

	public void selectAction() {

		if (!(comboBox.getSelectedItem() instanceof Item)) {
			return;
		}
		selectedItem = (Item) comboBox.getSelectedItem();
		System.out.println(selectedItem);
		Provider provider = new Provider();
		provider.setId(selectedItem.getId());
		List<Provider> list = providerService.findProviders(provider);
		setData(list.get(0));
	}

	/**
	 * 给组件添加数据
	 * 
	 * @param c
	 */
	private void setData(Provider c) {
		ProviderNameTf.setText(c.getProviderName());
		addressTf.setText(c.getAddress());
		shortsTf.setText(c.getShorts());
		zipTf.setText(c.getZip());
		telTf.setText(c.getTelephone());
		faxTf.setText(c.getFax());
		contactsTf.setText(c.getContacts());
		phoneTf.setText(c.getTelephone());
		bankTf.setText(c.getBank());
		accountTf.setText(c.getAccount());
		emailTf.setText(c.getMail());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String target = e.getActionCommand();
		String id = selectedItem.getId();
		switch (target) {
		case "修改":
			if(ProviderNameTf.getText().trim().equals("")||
					addressTf.getText().trim().equals("")||
					shortsTf.getText().trim().equals("")||
					zipTf.getText().trim().equals("")||
					telTf.getText().trim().equals("")||
					faxTf.getText().trim().equals("")||
					contactsTf.getText().trim().equals("")||
					phoneTf.getText().trim().equals("")||
					emailTf.getText().trim().equals("")||
					bankTf.getText().trim().equals("")||
					accountTf.getText().trim().equals("")) {
				
				JOptionPane.showMessageDialog(null, "请填写全部信息","温馨提示",JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			Provider provider = new Provider(id, ProviderNameTf.getText().trim(), shortsTf.getText().trim(),
					addressTf.getText().trim(), zipTf.getText().trim(), telTf.getText().trim(), faxTf.getText().trim(),
					contactsTf.getText().trim(), phoneTf.getText().trim(), bankTf.getText().trim(),
					accountTf.getText().trim(), emailTf.getText().trim(), 1);
			boolean result = providerService.updateProvider(provider);
			if (result == true) {
				JOptionPane.showMessageDialog(null, "客户修改成功!", "提示", JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, "客户信息修改失败!", "警告", JOptionPane.WARNING_MESSAGE);

			}
			break;
		case "删除":
			// 0 是 ，1 否
			int type = JOptionPane.showConfirmDialog(null, "是否删除?", "温馨提示", JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			if (type == 0) {
				 providerService.updateProvider(id, type);
				initComboBox();
			}

		default:
			break;
		}

	}
}
